PROJECT API
===========

Written by Greg Harvey.
http://www.drupaler.co.uk / http://www.cmspros.co.uk

WHY DOES THIS MODULE EXIST?
===========================

While creating the Drush Make Builder module I needed a way to fetch all the data 
about Drupal contrib projects from Drupal CVS, in order to allow people to have a
checkbox module chooser for building make files. The Plugin Manager module actually
had the structure to achieve this, but I didn't want most of it - just the bits 
dealing with retrieving and storing of the project data.

I decided to make those aspects from scratch, using Plugin Manager as a big 
influence (in some cases ripping entire functions), but so as the whole thing is:

a) Standalone and dedicated to importing repository data.
b) Abstract enough that it doesn't have to be Drupal CVS.
c) Pluggable! There are hooks here to add your own repositories.

Hopefully the Plugin Manager guys (and anyone else who needs a similar thing) will
use Project API going forwards.

HOW DO I USE IT?
================

Before you start, you *may* need to up your PHP memory to something higher than 
128MB. It depends on the size of the repository file the module needs to process.

Install and enable in the usual way. Make sure you install at least one pluggable
repository module or nothing will happen (in early versions there is only one 
pluggable repository module - Project API Drupal - make sure you enable this too).

In essence this is a developer tool. There's virtually no UI yet. All you can do 
is observe the raw data at admin/build/projects/test and refresh the Drupal 
project data at admin/build/project/rebuild. Note, both can take a while to load.

Other than this, there are a few API functions which are documented in-line
and you can look at the 'contrib' folder, which contains an example of creating
a module to extend Project API in the form of the Project API Drupal module.

WHAT IS THE FUTURE?
===================

Nearest on the horizon are some API helper functions to return projects by type
and by taxonomy and an admin interface to allow users to choose from the various
enabled pluggable repositories. These will be available very soon, because I
need them! Everything else is up to you. Please raise issues, supply patches, etc.
if you think this is a good idea and want to drive it on.
